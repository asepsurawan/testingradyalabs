﻿using App.Applications.DtoCollection.Users;
using App.Applications.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    [Authorize]
    public class UserManagerController : Controller
    {
        private readonly IUsersServices _repoUser;

        public UserManagerController(IUsersServices _repoUser)
        {
            this._repoUser = _repoUser;
        }

        // GET: AdminPanel/UserManager
        public ActionResult Index()
        {
            var data = _repoUser.GetList();
            return View(data.listObj);
        }

        // GET: AdminPanel/UserManager/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AdminPanel/UserManager/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdminPanel/UserManager/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminPanel/UserManager/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AdminPanel/UserManager/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminPanel/UserManager/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AdminPanel/UserManager/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
