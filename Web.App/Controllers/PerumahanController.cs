﻿using App.Applications.DtoCollection.Perumahan;
using App.Applications.DtoCollection.Perumahan.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    [Authorize]
    public class PerumahanController : Controller
    {
        private readonly IPerumahanServices _service;

        public PerumahanController(IPerumahanServices _service)
        {
            this._service = _service;
        }
        // GET: Perumahan
        public ActionResult Index()
        {
            ViewBag.JenisRUmah = Enum.GetValues(typeof(TypeRumah));
            return View(_service.GetList().listObj);
        }

        public ActionResult Load()
        {
            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();

            //find search columns info
            var typeRumah = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var search = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();

            var get = _service.GetListPageDataTable(new GetDataTable
            {
                draw = draw,
                start = start,
                length = length,
                typeRumah = !string.IsNullOrEmpty(typeRumah) ? Convert.ToInt16(typeRumah) : default(int?),
                search = search,
            });

            return Json(new
            {
                draw = get.draw,
                recordsFiltered = get.recordsFiltered,
                recordsTotal = get.recordsTotal,
                data = get.data,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadMap(int? type,string search)
        {
            var query = _service.GetQuery();
            if (type.HasValue)
            {
                query = query.Where(p=>p.typeRumah==type.Value);
            }
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(p => p.alamatRumah.Contains(search.ToLower()));
            }

            var data = query.ToList();
            return Json(new
            {
                dataMap = data
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CariRute(List<int> Id)
        {
            var data = new List<PerumahanViewModel>();
            if (Id.Count > 0)
            {
                foreach (var item in Id)
                {
                    var single = _service.GetDetails(item);
                    if (single!=null)
                    {
                        data.Add(single.obj);
                    }
                }
            }
            return View(data);
        }

        public ActionResult LoadRute(string lat,string lon)
        {
            return Json(new { });
        }

        // GET: Perumahan/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var perum = _service.GetDetails(id.Value);
            if (perum.obj == null)
            {
                return HttpNotFound();
            }
            return View(perum.obj);
        }

        // GET: Perumahan/Create
        public ActionResult Create()
        {
            ViewBag.JenisRUmah = Enum.GetValues(typeof(TypeRumah));
            return View();
        }

        // POST: Perumahan/Create
        [HttpPost]
        public ActionResult Create(PerumahanViewModel model)
        {
            if (ModelState.IsValid)
            {
                var add = _service.Create(model);
                if (add.status.Succeeded)
                {
                    TempData["code"] = "200";
                    TempData["msg"] = "Data Berhasil Di simpan";
                }
                else
                {
                    TempData["code"] = "500";
                    TempData["msg"] = add.status.description;
                }
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Perumahan/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var get = _service.GetDetails(id.Value);
            if (!get.status.Succeeded)
            {
                return HttpNotFound();
            }
            ViewBag.JenisRUmah = Enum.GetValues(typeof(TypeRumah));
            return View(get.obj);
        }

        // POST: Perumahan/Edit/5
        [HttpPost]
        public ActionResult Edit(PerumahanViewModel model)
        {
            if (ModelState.IsValid)
            {
                var add = _service.Update(model);
                if (add.status.Succeeded)
                {
                    TempData["code"] = "200";
                    TempData["msg"] = "Data Berhasil Di Ubah";
                }
                else
                {
                    TempData["code"] = "500";
                    TempData["msg"] = add.status.description;
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }

        //// GET: Perumahan/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var get = _service.GetDetails(id.Value);
        //    if (!get.status.Succeeded)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(get.obj);
        //}

        // POST: Perumahan/Delete/5
        public ActionResult Delete(int id)
        {
            var add = _service.Delete(id);
            if (add.status.Succeeded)
            {
                TempData["code"] = "200";
                TempData["msg"] = "Data Berhasil Di Hapus";
            }
            else
            {
                TempData["code"] = "500";
                TempData["msg"] = add.status.description;
            }
            return RedirectToAction("Index");
        }
    }
}
