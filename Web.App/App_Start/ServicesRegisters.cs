﻿
using App.Applications.DtoCollection.Users;
using App.Data.Infra;
using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Web.App.App_Start
{
    public class ServicesRegisters
    {
        public static void Run()
        {
            SetAutofactContainer();
        }

        public static void SetAutofactContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterGeneric(typeof(GenericRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();

            //Services
            builder.RegisterAssemblyTypes(typeof(UsersServices).Assembly)
               .Where(t => t.Name.EndsWith("Services"))
               .AsImplementedInterfaces().InstancePerRequest();

               //Services
               //builder.RegisterAssemblyTypes(typeof(MessageService).Assembly)
               //   .Where(t => t.Name.EndsWith("Services"))
               //   .AsImplementedInterfaces().InstancePerRequest();

            //builder.RegisterType<MessageService>().As<IMessageServices>()
            //    .SingleInstance().PreserveExistingDefaults();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}