﻿using App.Data.Datas;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Data.Infra
{
    public partial class GenericRepository<T> : IRepository<T> where T : class
    {
        private PerumahanDBEntities _context = new PerumahanDBEntities();

        public IQueryable<T> GetTable => _context.Set<T>().AsNoTracking();

        /// <summary>
        /// Get full error
        /// </summary>
        /// <param name="exc">Exception</param>
        /// <returns>Error</returns>
        protected string GetFullErrorText(DbEntityValidationException exc)
        {
            var msg = string.Empty;
            foreach (var validationErrors in exc.EntityValidationErrors)
                foreach (var error in validationErrors.ValidationErrors)
                    msg += string.Format("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage) + Environment.NewLine;
            return msg;
        }

        public virtual void Add(T entity)
        {
            try
            {
                _context.Set<T>().Add(entity);
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }

        }

        public virtual void Add(IEnumerable<T> entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entities");

                foreach (var item in entity)
                    _context.Set<T>().Add(item);

                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public virtual void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }

        public virtual void Delete(IEnumerable<T> entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entities");

                foreach (var item in entity)
                    _context.Set<T>().Remove(item);

                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = _context.Set<T>().Where(where).AsEnumerable();
            foreach (T obj in objects)
                _context.Set<T>().Remove(obj);
            _context.SaveChanges();
        }

        public virtual T Get(Expression<Func<T, bool>> where)
        {
            return _context.Set<T>().Where(where).FirstOrDefault<T>();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public virtual T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return _context.Set<T>().Where(where).ToList();
        }

        public virtual void Update(T entity)
        {
            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public virtual void Update(IEnumerable<T> entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entities");

                foreach (var item in entity)
                    _context.Set<T>().Attach(item);

                _context.Entry(entity).State = EntityState.Modified;

                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new Exception(GetFullErrorText(dbEx), dbEx);
            }
        }
    }
}
