﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Data.Infra
{
    public interface IRepository<TObject> where TObject : class
    {
        /// <summary>
        /// Satu Kali Add Data
        /// </summary>
        /// <param name="entity"></param>
        void Add(TObject entity);

        /// <summary>
        /// ADD data untuk banyak data
        /// </summary>
        /// <param name="entity"></param>
        void Add(IEnumerable<TObject> entity);

        /// <summary>
        /// Update Data 
        /// </summary>
        /// <param name="entity"></param>
        void Update(TObject entity);

        /// <summary>
        /// Update Banyak Data 
        /// </summary>
        /// <param name="entity"></param>
        void Update(IEnumerable<TObject> entity);

        /// <summary>
        /// Remove data
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TObject entity);

        /// <summary>
        /// Remove Banyak data
        /// </summary>
        /// <param name="entity"></param>
        void Delete(IEnumerable<TObject> entity);

        /// <summary>
        /// Remove data Where
        /// </summary>
        /// <param name="where"></param>
        void Delete(Expression<Func<TObject, bool>> where);

        /// <summary>
        ///  Get Data by int id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TObject GetById(int id);

        /// <summary>
        /// Get Data Where
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        TObject Get(Expression<Func<TObject, bool>> where);

        /// <summary>
        /// Gets all Data 
        /// </summary>
        /// <returns></returns>
        IEnumerable<TObject> GetAll();

        /// <summary>
        /// Get All Data Where
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        IEnumerable<TObject> GetMany(Expression<Func<TObject, bool>> where);

        IQueryable<TObject> GetTable { get; }
    }
}
