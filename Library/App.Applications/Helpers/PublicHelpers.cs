﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace App.Applications.Helpers
{
    public class PublicHelpers
    {

    }

    public class ApplicationRoles
    {
        public const string Admin = "Administrator";
        public const string participan = "Participan";
        public static string[] AllRoles
        {
            get { return new string[] { Admin, participan }; }
        }
    }

    /// <summary>
    /// check file size image one file in for model
    /// </summary>
    public class FileSizeAttribute : ValidationAttribute
    {
        private readonly int _maxSize;

        public FileSizeAttribute(int maxSize)
        {
            _maxSize = maxSize;
        }
        public override bool IsValid(object value)
        {
            if (value == null) return true;
            // List<object> data =new List<object>(value);
            return (value as HttpPostedFileBase).ContentLength <= _maxSize;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("The file size should not exceed {0}", _maxSize);
        }
    }

    /// <summary>
    /// check file type extention one file in for model
    /// </summary>
    public class FileTypesAttribute : ValidationAttribute
    {
        private readonly List<string> _types;

        public FileTypesAttribute(string types)
        {
            _types = types.Split(',').ToList();
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;

            var fileExt = System.IO.Path.GetExtension((value as HttpPostedFileBase).FileName);
            return _types.Contains(fileExt, StringComparer.OrdinalIgnoreCase);
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("Invalid file type. Only the following types {0} are supported.", String.Join(", ", _types));
        }
    }

    public class HelperUploadFile
    {
        /// <summary>
        /// Upload Image To Folder Server And Return File Name For Saving In Database
        /// </summary>
        /// <param name="image"></param>
        /// <param name="folderPath"></param>
        /// <returns>File Name For Saving In Database</returns>
        public string UploadImage(HttpPostedFileBase image, string folderPath)
        {
            if (image != null && folderPath != null)
            {
                //string folderPath = Server.MapPath(ConfigurationSettings.AppSettings["ImageProduct"].ToString());
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                var fileExt = System.IO.Path.GetExtension(image.FileName);
                string fileName = "IMG_" + Guid.NewGuid() + fileExt;
                image.SaveAs(folderPath + fileName);

                return fileName;
            }
            return null;
        }

        /// <summary>
        /// Update And Upload Image To Folder Server And Return File Name For Saving In Database
        /// </summary>
        /// <param name="image"></param>
        /// <param name="folderPath"></param>
        /// <param name="imageInDatabase"></param>
        /// <returns>File Name For Saving In Database</returns>
        public string UploadImage(HttpPostedFileBase image, string folderPath, string imageInDatabase)
        {
            if (image != null && imageInDatabase != null)
            {
                if (image.FileName != imageInDatabase)
                {
                    FileInfo fileDel = new FileInfo(folderPath + imageInDatabase);
                    if (fileDel.Exists)
                    {
                        fileDel.Delete();
                    }
                    //string folderPath = Server.MapPath(ConfigurationSettings.AppSettings["ImageProduct"].ToString());
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    var fileExt = System.IO.Path.GetExtension(image.FileName);
                    string fileName = "IMG_" + Guid.NewGuid() + fileExt;
                    image.SaveAs(folderPath + fileName);

                    return fileName;
                }
                return imageInDatabase;
            }
            return imageInDatabase;
        }

        /// <summary>
        /// Delete Image 
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="ImageName"></param>
        /// <returns></returns>
        public bool DeleteImage(string folderPath, string ImageName)
        {
            var result = false;
            if (folderPath != null && ImageName != null)
            {
                try
                {
                    FileInfo fileDel = new FileInfo(folderPath + ImageName);
                    if (fileDel.Exists)
                    {
                        fileDel.Delete();
                    }
                    result = true;
                    return result;
                }
                catch (Exception ex)
                {
                    return result;
                }
            }
            return result;
        }

        //public System.Drawing.Image Base64ToImage(string base64)
        //{
        //    try
        //    {
        //        byte[] imageBytes = Convert.FromBase64String(base64);
        //        MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
        //        ms.Write(imageBytes, 0, imageBytes.Length);
        //        System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
        //        return image;
        //    }
        //    catch
        //    {
        //        return null;
        //    }

        //}
    }
}
