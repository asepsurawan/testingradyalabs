﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Applications.Helpers
{
    public class ServiceResult
    {
        public ServiceResult()
        {
            this.BadRequest();
        }
        public int code { get; set; }
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public string description { get; set; }


        public void OK()
        {
            this.code = 200;
            this.Succeeded = true;
            this.Message = "All OK";
        }
        public void Created()
        {
            this.code = 201;
            this.Succeeded = true;
            this.Message = "New object saved";
        }
        public void Updated()
        {
            this.code = 202;
            this.Succeeded = true;
            this.Message = "Update object saved";
        }
        public void Deleted()
        {
            this.code = 203;
            this.Succeeded = true;
            this.Message = "Object deleted";
        }

        public void BadRequest()
        {
            this.code = 400;
            this.Succeeded = false;
            this.Message = "Bad Request";
        }
        public void UnAuthorized()
        {
            this.code = 401;
            this.Succeeded = false;
            this.Message = "Unauthorized";
        }
        public void NotFound()
        {
            this.code = 404;
            this.Succeeded = false;
            this.Message = "Not Found";
        }
        public void MethodNotAllowed()
        {
            this.code = 405;
            this.Succeeded = false;
            this.Message = "Method Not Allowed";
        }
        public void Error(string message)
        {
            this.code = 500;
            this.Succeeded = false;
            this.Message = "Internal Server Error";
            this.description = "Failed With Error : " + message;
        }
    }

    public class Results<TObject>
    {
        public Results()
        {
            status = new ServiceResult();
            listObj = new List<TObject>();
        }
        public TObject obj { get; set; }
        public List<TObject> listObj { get; set; }
        public ServiceResult status { get; set; }
    }


    public class ResultDataTable<TObject>
    {
        public ResultDataTable()
        {
            status = new ServiceResult();
            data = new List<TObject>();
        }
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<TObject> data { get; set; }
        public ServiceResult status { get; set; }
    }
}
