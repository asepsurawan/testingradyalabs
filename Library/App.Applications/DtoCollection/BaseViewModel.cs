﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Applications.DtoCollection
{
    public class BaseViewModel
    {
        public int Id { get; set; }
        public string createDate { get; set; }
        public string updateDate { get; set; }
        public string createBy { get; set; }
        public string updateBy { get; set; }
    }
}
