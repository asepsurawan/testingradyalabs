﻿using App.Applications.DtoCollection.Users.Dto;
using App.Applications.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Applications.DtoCollection.Users
{
    public interface IUsersServices
    {
        Results<MasterUser> GetList();
        Results<MasterUser> GetDetails(string id);
        Results<MasterUser> UpdateImageAvatar(MasterUser master);
        ResultDataTable<MasterUser> GetListPageDataTable(MasterUserDataTableParam query);
    }
}
