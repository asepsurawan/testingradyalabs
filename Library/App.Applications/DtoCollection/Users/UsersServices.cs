﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Applications.DtoCollection.Users.Dto;
using App.Applications.Helpers;
using App.Data.Datas;
using App.Data.Infra;

namespace App.Applications.DtoCollection.Users
{
    public class UsersServices : IUsersServices
    {
        private readonly IRepository<AspNetUsers> _repoUsers;

        public UsersServices(IRepository<AspNetUsers> _repoUsers)
        {
            this._repoUsers = _repoUsers;
        }
        public Results<MasterUser> GetDetails(string id)
        {
            var result = new Results<MasterUser>();
            var defaults= new List<string>();
            try
            {
                result.obj = _repoUsers.GetTable.Where(d=> d.Id==id).Select(p=> new MasterUser
                {
                    Id=p.Id,
                    Username=p.UserName,
                    email=p.Email,
                    isActive=p.LockoutEnabled,
                    roles=p.AspNetRoles!=null?p.AspNetRoles.Select(x=> x.Name).ToList(): new List<string>(),
                }).FirstOrDefault();

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }

        public Results<MasterUser> GetList()
        {
            var result = new Results<MasterUser>();
            try
            {
                result.listObj = _repoUsers.GetAll().Select(p => new MasterUser
                {
                    Id = p.Id,
                    Username = p.UserName,
                    email = p.Email,
                    isActive = p.LockoutEnabled,
                    roles = p.AspNetRoles != null ? p.AspNetRoles.Select(x => x.Name).ToList() : new List<string>(),
                }).ToList();

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }

        public ResultDataTable<MasterUser> GetListPageDataTable(MasterUserDataTableParam query)
        {
            var result = new ResultDataTable<MasterUser>();
            try
            {
                result.data = _repoUsers.GetAll().Select(p => new MasterUser
                {
                    Id = p.Id,
                    Username = p.UserName,
                    email = p.Email,
                    isActive = p.LockoutEnabled,
                    roles = p.AspNetRoles != null ? p.AspNetRoles.Select(x => x.Name).ToList() : new List<string>(),
                }).ToList();

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }


        public Results<MasterUser> UpdateImageAvatar(MasterUser master)
        {
            var result = new Results<MasterUser>();
            try
            {
                var obj = _repoUsers.GetTable.Where(d => d.Id == master.Id).FirstOrDefault();
                var folderPath = System.Web.HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["Avatar"].ToString());
                obj.UserAvatar= new HelperUploadFile().UploadImage(master.imageFile, folderPath);
                _repoUsers.Update(obj);
                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }
    }
}
