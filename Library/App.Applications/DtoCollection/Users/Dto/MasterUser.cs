﻿using App.Applications.Helpers;
using System;
using System.Collections.Generic;
using System.Web;

namespace App.Applications.DtoCollection.Users.Dto
{
    public class MasterUser
    {
        public string Id { get; set; }
        public string email { get; set; }
        public string Username { get; set; }
        public List<string> roles { get; set; }
        public bool isActive { get; set; }
        public string phone { get; set; }
        public string fulname { get; set; }
        public string imageAvatarName { get; set; }
        [FileTypes(".jpg,.jpeg,.png")]
        public HttpPostedFileBase imageFile { get; set; }

    }

    public class MasterUserDataTableParam
    {
        public string draw { get; set; }
        public string start { get; set; }
        public string length { get; set; }
        public string roles { get; set; }
        public string userName { get; set; }
    }
}
