﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Applications.DtoCollection.Perumahan.Dto;
using App.Applications.Helpers;
using App.Data.Datas;
using App.Data.Infra;

namespace App.Applications.DtoCollection.Perumahan
{
    public class PerumahanServices : IPerumahanServices
    {
        private readonly IRepository<TPerumahan> _repo;
        private string folderPath;
        private string pathShow;
        public PerumahanServices(IRepository<TPerumahan> _repo)
        {
            this._repo = _repo;
            folderPath = System.Web.HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["ImageRumah"].ToString());
            pathShow = ConfigurationSettings.AppSettings["ImageRumah"].ToString();
            pathShow = pathShow.Replace("~", "");
        }

        public Results<PerumahanViewModel> CariRute(string latStart, string lonStart, string latEnd, string lonEnd)
        {
            throw new NotImplementedException();
        }

        public Results<PerumahanViewModel> Create(PerumahanViewModel data)
        {
            var result = new Results<PerumahanViewModel>();
            try
            {
                var obj = new App.Data.Datas.TPerumahan();
                obj.alamatRumah = data.alamat_rumah;
                obj.Lat = data.lat;
                obj.Lon = data.lon;
                obj.typeRumah = data.typeRumah;
                obj.imageRumah = new HelperUploadFile().UploadImage(data.imageFile, folderPath);
                obj.createBy = data.createBy;
                obj.createDate = DateTime.Now;

                _repo.Add(obj);

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }

        public Results<PerumahanViewModel> Delete(int Id)
        {
            var result = new Results<PerumahanViewModel>();
            try
            {
                var obj = _repo.GetById(Id);
                var deleteImg = new HelperUploadFile().DeleteImage(folderPath, obj.imageRumah);
                _repo.Delete(obj);

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }
        

        public Results<PerumahanViewModel> GetDetails(int id)
        {
            var result = new Results<PerumahanViewModel>();
            try
            {
                var data = _repo.GetById(id);
                var obj = new PerumahanViewModel();
                obj.alamat_rumah = data.alamatRumah;
                obj.lat = data.Lat;
                obj.lon = data.Lon;
                obj.typeRumah = data.typeRumah;
                obj.imageRumah = data.imageRumah != null ? pathShow + data.imageRumah : "";
                obj.createBy = data.createBy;
                obj.createDate = data.createDate.Value.ToString("dd MMM yyyy");
                obj.updateBy = data.updateBy;
                obj.updateDate = data.updateDate.HasValue ? data.updateDate.Value.ToString("dd MMM yyyy") : "";

                result.obj = obj;

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }

        public Results<PerumahanViewModel> GetList()
        {
            var result = new Results<PerumahanViewModel>();
            try
            {
                result.listObj = _repo.GetTable.Select(p => new PerumahanViewModel
                {
                    Id = p.Id,
                    alamat_rumah = p.alamatRumah,
                    lat = p.Lat,
                    lon = p.Lon,
                    typeRumah = p.typeRumah,
                    imageRumah = p.imageRumah != null ? pathShow + p.imageRumah : "",
                    createBy = p.createBy,
                    createDate = p.createDate.Value.ToString("dd MMM yyyy"),
                    updateBy = p.updateBy,
                    updateDate = p.updateDate.HasValue ? p.updateDate.Value.ToString("dd MMM yyyy") : "",
            }).ToList();

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }

        public ResultDataTable<PerumahanViewModel> GetListPageDataTable(GetDataTable query)
        {
            var result = new ResultDataTable<PerumahanViewModel>();
            try
            {
                int pageSize = query.length != null ? Convert.ToInt32(query.length) : 0;
                int skip = query.start != null ? Convert.ToInt16(query.start) : 0;

                var Asquery = _repo.GetTable;
                
                if (query.typeRumah.HasValue)
                {
                    Asquery = Asquery.Where(p => p.typeRumah == query.typeRumah.Value);
                }

                if (!string.IsNullOrEmpty(query.search))
                {
                    Asquery = Asquery.Where(p => p.alamatRumah.Contains(query.search));
                }

                result.recordsTotal = Asquery.Count();
                result.draw = query.draw;
                result.recordsFiltered = result.recordsTotal;
                result.data = Asquery.OrderBy(p => p.Id).Skip(skip).Take(pageSize).ToList().Select(p => new PerumahanViewModel
                {
                    Id = p.Id,
                    alamat_rumah = p.alamatRumah,
                    lat = p.Lat,
                    lon = p.Lon,
                    typeRumah = p.typeRumah,
                    imageRumah = p.imageRumah != null ? pathShow + p.imageRumah : "",
                    createBy = p.createBy,
                    createDate = p.createDate.Value.ToString("dd MMM yyyy"),
                    updateBy = p.updateBy,
                    updateDate = p.updateDate.HasValue ? p.updateDate.Value.ToString("dd MMM yyyy") : "",
                }).ToList();

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }

        public IQueryable<TPerumahan> GetQuery()
        {
            var result = _repo.GetTable;

            return result;
        }

        public Results<PerumahanViewModel> Update(PerumahanViewModel data)
        {
            var result = new Results<PerumahanViewModel>();
            try
            {
                var obj = _repo.GetById(data.Id);
                obj.alamatRumah = data.alamat_rumah;
                obj.Lat = data.lat;
                obj.Lon = data.lon;
                obj.typeRumah = data.typeRumah;
                obj.imageRumah = new HelperUploadFile().UploadImage(data.imageFile, folderPath, obj.imageRumah);
                obj.updateBy = data.createBy;
                obj.updateDate = DateTime.Now;

                _repo.Update(obj);

                result.status.OK();
            }
            catch (Exception ex)
            {
                result.status.Error(ex.Message);
            }
            return result;
        }
        
    }
}
