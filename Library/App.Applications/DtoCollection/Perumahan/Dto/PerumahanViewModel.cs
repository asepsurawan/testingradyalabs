﻿using App.Applications.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace App.Applications.DtoCollection.Perumahan.Dto
{
    public class PerumahanViewModel:BaseViewModel
    {
        public string imageRumah { get; set; }
        public string alamat_rumah { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public int? typeRumah { get; set; }
        [FileSize(202400)]
        [FileTypes(".jpg,.jpeg,.png")]
        public HttpPostedFileBase imageFile { get; set; }
    }


    public enum TypeRumah
    {
        type1=34,
        type2=45,
        type3=72
    }

    public class GetDataTable
    {
        public string draw { get; set; }
        public string start { get; set; }
        public string length { get; set; }
        public int? typeRumah { get; set; }
        public string search { get; set; }
    }
}
