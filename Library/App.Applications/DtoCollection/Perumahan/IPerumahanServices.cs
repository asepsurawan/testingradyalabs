﻿using App.Applications.DtoCollection.Perumahan.Dto;
using App.Applications.Helpers;
using App.Data.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Applications.DtoCollection.Perumahan
{
    public interface IPerumahanServices
    {
        Results<PerumahanViewModel> GetList();
        Results<PerumahanViewModel> GetDetails(int id);
        Results<PerumahanViewModel> Create(PerumahanViewModel data);
        Results<PerumahanViewModel> Update(PerumahanViewModel data);
        Results<PerumahanViewModel> Delete(int Id);
        Results<PerumahanViewModel> CariRute(string latStart,string lonStart,string latEnd,string lonEnd);
        ResultDataTable<PerumahanViewModel> GetListPageDataTable(GetDataTable query);
        IQueryable<TPerumahan> GetQuery();
    }
}
